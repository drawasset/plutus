
from impedev import tradingview
from impedev import binance
from impedev import export_excel as ee
from impedev import log
import time
import datetime
import schedule


lg = log.log(datetime.datetime.now().strftime(
    ("%H-%M-%S")) + 'log.csv')

# Initial Tradingview
tv = tradingview.tradingview(_headless=False, _window_size="--start-maximized")
tv.start()
time.sleep(1)
tv.login()
time.sleep(3)
tv.active_data_window()
time.sleep(1)
tv.import_indicators(['SSL HYBRID', 'QQE MOD'])
tv.get_indicator_data()
tv.input_symbol(market='Crypto', symbol="BINANCE:ETHUSDTPERP")
time.sleep(1)
tv.input_timeframe('5')


_cur_minute = 1
_action_state = "None"
_coming_signal = "None"
histo_ratio = 0


def run_bot(action_state, cur_minute, coming_signal):
    ind_data = tv.get_indicator_data()
    print(ind_data['exit_arrows'])
    state, signal = ind_data['exit_arrows']
    if state and signal != 'n/a':
        if '−' in signal:
            coming_signal = "Short"
        elif float(signal) == 1:
            coming_signal = "Long"

    state, histo = ind_data['histo2']
    if state:
        if '−' in histo:
            buff = float(histo.replace('−', ''))
            histo = -buff
        else:
            histo = float(histo)

    else:
        histo = 0.00

    lg.log('Debug', "Histo: " + ind_data['histo2'][1])

    # Is not in action state
    if action_state == "None":
        if cur_minute >= 3 and cur_minute <= 5:
            if coming_signal == "Short":
                if histo < -histo_ratio:
                    action_state = "Short"
                    # Binance short @ market
                    lg.log('Debug', datetime.datetime.now().strftime(
                        ("%m/%d/%Y, %H:%M:%S")) + ' : ' + "Enter " + action_state)

                    # Is order is complete

            if coming_signal == "Long":
                if histo > histo_ratio:
                    action_state = "Long"
                    # Binance Long @ market
                    lg.log('Debug', datetime.datetime.now().strftime(
                        ("%m/%d/%Y, %H:%M:%S")) + ' : ' + "Enter " + action_state)
                    # Is order is complete

    # Is  in action state
    elif action_state == "Short":
        if cur_minute >= 2 and cur_minute <= 5:
            if coming_signal == "Long":
                # Binance Long @ market
                # Is order is complete
                lg.log('Debug', datetime.datetime.now().strftime(
                    ("%m/%d/%Y, %H:%M:%S")) + ' : ' + "Exit Short")

                if histo > histo_ratio:
                    action_state = "Long"
                    # Binance Long @ market
                    lg.log('Debug', datetime.datetime.now().strftime(
                        ("%m/%d/%Y, %H:%M:%S")) + ' : ' + action_state)
                    # Is order is complete
                else:
                    action_state = "None"

    elif action_state == "Long":
        if cur_minute >= 2 and cur_minute <= 5:
            if coming_signal == "Short":
                # Binance Long @ market
                # Is order is complete
                lg.log('Debug', datetime.datetime.now().strftime(
                    ("%m/%d/%Y, %H:%M:%S")) + ' : ' + "Exit Long")

                if histo < -histo_ratio:
                    action_state = "Short"
                    # Binance Short @ market
                    lg.log('Debug', datetime.datetime.now().strftime(
                        ("%m/%d/%Y, %H:%M:%S")) + ' : ' + action_state)
                    # Is order is complete
                else:
                    action_state = "None"

    cur_minute = cur_minute + 1
    if cur_minute > 5:
        cur_minute = 1

    return action_state, cur_minute, coming_signal


for i in range(100):

    _action_state, _cur_minute, _coming_signal = run_bot(
        _action_state, _cur_minute, _coming_signal)

    now_str = datetime.datetime.now().strftime(("%m/%d/%Y, %H:%M:%S"))
    lg.log('After', now_str + " | Action: " + _action_state + " | Signal: " +
           _coming_signal + " | Time: " + str(_cur_minute))

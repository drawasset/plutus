'''
------------------ Plutus -----------------
Copyright   : ApiwatDrawasset
Author      : Apiwat Pitaksin
Date        : 2021/08/01
Description : Trading Bot Version Beta 1.0.0
-------------------------------------------
'''

# To do :
# 1) Exit by stoploss
# 2) Check tradingview xpath
# 3) For now need to manually start bot with candle timeframe
#    Find how to get candle real-time data (maybe by web socket)
# 4) To do check if there any order in positions,
#    if empty action_state can be set to None.

import os
import sys
import pandas as pd
from impedev import tradingview
from impedev import binance
from impedev import export_excel as ee
from impedev import log
import time
import datetime
import msvcrt

# Constant
EXIT_ARROW_INDEX = 6
HISTO2_INDEX = 17
TIMEFRAME = 180         # mins
HISTO_RATIO = 0

# Initial Symbol and amount
# , "BTC/USDT"]  # , "AXS/USDT", "LTC/USDT"]
symbols = ["ETH/USDT", "BNB/USDT", "BTC/USDT",
           "AXS/USDT", "LTC/USDT", "XRP/USDT", "ADA/USDT"]
amounts = [0.03, 0.2, 0.002, 1, 0.2, 15, 7]

cur_dir = os.getcwd()

# Initial Log


def initial_log():
    log_filename = datetime.datetime.now().strftime(("%m%d%Y_%H-%M-%S")) + "_log.csv"
    log_dir = os.path.join(cur_dir, 'log')
    if not os.path.isdir(log_dir):
        os.makedirs(log_dir)
    lg = log.log(os.path.join(log_dir, log_filename))
    return lg


def initial_tradingview():
    lg.log("Notice", "Initialing Tradingview... ")
    tv = tradingview.tradingview(
        _headless=True, _window_size="--start-maximized")
    tv.start()
    time.sleep(1)
    tv.login()
    time.sleep(3)
    tv.active_data_window()
    time.sleep(1)
    tv.import_indicators(["SSL HYBRID", "QQE MOD"])
    tv.get_indicator_data()
    tv.input_symbol(market="Crypto", symbol="BINANCE:ETHUSDTPERP")
    time.sleep(1)
    tv.input_timeframe(str(TIMEFRAME))

    iscorrect = False
    status, text = tv.get_current_symbol()
    if text == "ETHUSDTPERP":
        iscorrect = True
    else:
        tv = False

    return tv


def read_tail(fn, n):
    # Open the file and get all the lines in a list
    with open(fn, 'r') as f:
        f.readline()
        lines = f.readlines()

    # Return a string as an array.By the way str->Type convert to float
    return [list(map(float, line.strip().split(','))) for line in lines[-n:]]


def get_signal(symbol):

    iscorrect = False
    compare_text = symbol[0:symbol.index("/")] + "USDTPERP"
    while not iscorrect:

        tv.input_symbol(market="Crypto", symbol=compare_text)
        time.sleep(3)
        status, text = tv.get_current_symbol()
        if text == compare_text:
            iscorrect = True
            break

    filename = os.path.join(
        tv.chrome_download_dir, "BINANCE_" +
        symbol[0:symbol.index("/")] + "USDTPERP, " + str(TIMEFRAME) + ".csv")

    signal = {}
    counting = 0
    issuccess = False

    while not issuccess:
        isdownload = tv.download_csv()
        try:
            if isdownload:
                while not os.path.isfile(filename):
                    counting = counting + 1
                    time.sleep(0.1)
                    if(counting >= 50):
                        lg.log("Warning", "Download CSV Failed.")
                        break
                if os.path.isfile(filename):
                    line = read_tail(filename, 2)
                    signal['action_signal'] = line[0][EXIT_ARROW_INDEX]
                    signal['histo2_signal'] = line[1][HISTO2_INDEX]
                    os.remove(filename)
                    issuccess = True
                else:
                    lg.log("Warning", "Missed Signal.")
        except:
            issuccess = False
            lg.log("Warning", "Could not get signal! " + filename)

    return signal


def run_bot(symbol, amount, action_state, coming_signal):

    signal = get_signal(symbol)
    if signal['action_signal'] == -1:
        coming_signal = "Short"
    elif signal['action_signal'] == 1:
        coming_signal = "Long"

    histo = signal['histo2_signal']

    lg.log("Debug", symbol + " | Histo: " + str(histo))

    # Is not in action state
    if action_state == "None":
        if coming_signal == "Short":
            if histo < -HISTO_RATIO:
                action_state = "Short"
                # Binance short @ market
                try:
                    order = bn.place_short_market(symbol, amount)
                    lg.log("Notice", "OrderID: " + order["id"] +
                           " | Symbol: " + order["symbol"] +
                           " | Side: " + order["side"] +
                           " | Price: " + str(order["price"]) +
                           " | Cost: " + str(order["cost"]) +
                           " | Amount: " + str(order["amount"]))

                    lg.log("Debug",  symbol +
                           " | " + "Enter " + action_state)
                except:
                    lg.log("Warning",  symbol +
                           " | " + "Ordering Failed " + action_state)

                # Todo: Is order is complete

        if coming_signal == "Long":
            if histo > HISTO_RATIO:
                action_state = "Long"
                # Binance Long @ market
                try:
                    order = bn.place_long_market(symbol, amount)
                    lg.log("Notice", "OrderID: " + order["id"] +
                           " | Symbol: " + order["symbol"] +
                           " | Side: " + order["side"] +
                           " | Price: " + str(order["price"]) +
                           " | Cost: " + str(order["cost"]) +
                           " | Amount: " + str(order["amount"]))

                    lg.log("Debug",  symbol +
                           " | " + "Enter " + action_state)
                except:
                    lg.log("Warning",  symbol +
                           " | " + "Ordering Failed " + action_state)
                # Todo: Is order is complete

    # Is  in action state
    elif action_state == "Short":
        if coming_signal == "Long":
            # Binance Long @ market
            try:
                order = bn.place_long_market(symbol, amount)
                lg.log("Notice", "OrderID: " + order["id"] +
                       " | Symbol: " + order["symbol"] +
                       " | Side: " + order["side"] +
                       " | Price: " + str(order["price"]) +
                       " | Cost: " + str(order["cost"]) +
                       " | Amount: " + str(order["amount"]))

                # Todo: Is order is complete
                lg.log("Debug", symbol + " | " + "Exit Short")
            except:
                lg.log("Warning",  symbol +
                       " | " + "Ordering Failed " + action_state)

            if histo > HISTO_RATIO:
                action_state = "Long"
                # Binance Long @ market
                try:
                    order = bn.place_long_market(symbol, amount)
                    lg.log("Notice", "OrderID: " + order["id"] +
                           " | Symbol: " + order["symbol"] +
                           " | Side: " + order["side"] +
                           " | Price: " + str(order["price"]) +
                           " | Cost: " + str(order["cost"]) +
                           " | Amount: " + str(order["amount"]))

                    lg.log("Debug",  symbol +
                           " | " + "Enter " + action_state)
                except:
                    lg.log("Warning",  symbol +
                           " | " + "Ordering Failed " + action_state)
                # Todo: Is order is complete

            else:
                action_state = "None"

    elif action_state == "Long":
        if coming_signal == "Short":
            # Binance short @ market
            try:
                order = bn.place_short_market(symbol, amount)
                lg.log("Notice", "OrderID: " + order["id"] +
                       " | Symbol: " + order["symbol"] +
                       " | Side: " + order["side"] +
                       " | Price: " + str(order["price"]) +
                       " | Cost: " + str(order["cost"]) +
                       " | Amount: " + str(order["amount"]))

                # Todo: Is order is complete

                lg.log("Debug", symbol + " | " + "Exit Long")
            except:
                lg.log("Warning",  symbol +
                       " | " + "Ordering Failed " + action_state)

            if histo < -HISTO_RATIO:
                action_state = "Short"
                # Binance short @ market
                try:
                    order = bn.place_short_market(symbol, amount)
                    lg.log("Notice", "OrderID: " + order["id"] +
                           " | Symbol: " + order["symbol"] +
                           " | Side: " + order["side"] +
                           " | Price: " + str(order["price"]) +
                           " | Cost: " + str(order["cost"]) +
                           " | Amount: " + str(order["amount"]))

                    lg.log("Debug",  symbol +
                           " | " + "Enter " + action_state)
                except:
                    lg.log("Warning",  symbol +
                           " | " + "Ordering Failed " + action_state)
                    # Todo: Is order is complete
            else:
                action_state = "None"

    return action_state, coming_signal


lg = initial_log()
lg.log("Notice", "Starting Plutus... ")

tv = initial_tradingview()
if isinstance(tv, (int, float)):
    lg.log("Error", "Could not success log-in Tradingview... ")
    sys.exit(1)

bn = binance.binance()
# Initial Signal
_action_state = {}
_coming_signal = {}
for i in range(len(symbols)):
    _action_state[symbols[i]] = "None"
    _coming_signal[symbols[i]] = "None"


# Finding Sampling time list
hr_sampling = int(TIMEFRAME / 60)
day_sampling = int(24 / hr_sampling)
hr_list = []
for i in range(day_sampling):
    hr_list.append(i*hr_sampling)


isESCPressed = False
while not isESCPressed:
    isSync = False
    while not isSync:
        sec = datetime.datetime.now().second
        min = datetime.datetime.now().minute
        hr = datetime.datetime.now().hour

        if hr in hr_list:
            if min % TIMEFRAME == 0:
                if sec == 0:

                    for i in range(len(symbols)):
                        print("")
                        lg.log("Notice", "------------- " +
                               symbols[i] + " -------------")

                        _action_state[symbols[i]], _coming_signal[symbols[i]] = run_bot(symbols[i], amounts[i],
                                                                                        _action_state[symbols[i]], _coming_signal[symbols[i]])

                        lg.log("Notice", symbols[i] + " | After Calling" +
                               " | Action: " + _action_state[symbols[i]] +
                               " | Signal: " + _coming_signal[symbols[i]])
                    isSync = True
                    time.sleep(1.5)
                    break

        if msvcrt.kbhit():
            key_stroke = msvcrt.getche()
            if key_stroke == chr(27).encode():
                print("Esc key pressed")
                isESCPressed = True
                break
